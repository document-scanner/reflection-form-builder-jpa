/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.reflection.form.builder.jpa.storage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests {@link PostgresqlAutoPersistenceStorageConf}.
 */
public class PostgresqlAutoPersistenceStorageConfTest {

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void findBestInitialPostgresqlBasePathNull() throws IOException {
        Throwable thrown = assertThrows(IllegalArgumentException.class,
            () -> PostgresqlAutoPersistenceStorageConf.findBestInitialPostgresqlBasePath(null));
        assertTrue(thrown instanceof IllegalArgumentException);
        assertEquals("postgresqlDir mustn't be null",
                thrown.getMessage());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void findBestInitialPostgresqlBasePathInexisting() throws IOException {
        File postgresqlDir = Files.createTempDirectory(PostgresqlAutoPersistenceStorageConfTest.class.getSimpleName()).toFile();
        Throwable thrown = assertThrows(IllegalArgumentException.class,
            () -> {
                postgresqlDir.delete();
                PostgresqlAutoPersistenceStorageConf.findBestInitialPostgresqlBasePath(postgresqlDir);
            });
        assertTrue(thrown instanceof IllegalArgumentException);
        assertEquals(String.format("postgresqlDir '%s' doesn't exist",
                        postgresqlDir.getAbsolutePath()),
                thrown.getMessage());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void findBestInitialPostgresqlBasePathEmpty() throws IOException {
        File postgresqlDir = Files.createTempDirectory(PostgresqlAutoPersistenceStorageConfTest.class.getSimpleName()).toFile();
        Throwable thrown = assertThrows(PostgresqlInitialBasePathFindException.class,
            () -> {
                PostgresqlAutoPersistenceStorageConf.findBestInitialPostgresqlBasePath(postgresqlDir);
            });
        assertTrue(thrown instanceof PostgresqlInitialBasePathFindException);
        assertEquals(String.format("the postgresqlDir '%s' doesn't contain any directory matching '%s' or '%s' which can be used to find the PostgreSQL base directory with most recent version (note that whitespace and non-printable characters are not trimmed from the directory names)",
                        postgresqlDir.getAbsolutePath(),
                        PostgresqlAutoPersistenceStorageConf.VERSION_LE_9,
                        PostgresqlAutoPersistenceStorageConf.VERSION_GE_10),
                thrown.getMessage());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void findBestInitialPostgresqlBasePathMissingTrailingNumber() throws IOException {
        File postgresqlDir = Files.createTempDirectory(PostgresqlAutoPersistenceStorageConfTest.class.getSimpleName()).toFile();
        File missingNumberDir = new File(postgresqlDir, "1.");
        missingNumberDir.mkdir();
        Throwable thrown = assertThrows(PostgresqlInitialBasePathFindException.class,
            () -> {
                PostgresqlAutoPersistenceStorageConf.findBestInitialPostgresqlBasePath(postgresqlDir);
            });
        assertTrue(thrown instanceof PostgresqlInitialBasePathFindException);
        assertEquals(String.format("the postgresqlDir '%s' doesn't contain any directory matching '%s' or '%s' which can be used to find the PostgreSQL base directory with most recent version (note that whitespace and non-printable characters are not trimmed from the directory names)",
                postgresqlDir.getAbsolutePath(),
                PostgresqlAutoPersistenceStorageConf.VERSION_LE_9,
                PostgresqlAutoPersistenceStorageConf.VERSION_GE_10),
                thrown.getMessage());
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void findBestInitialPostgresqlBasePathMissingLeadingNumber() throws IOException {
        File postgresqlDir = Files.createTempDirectory(PostgresqlAutoPersistenceStorageConfTest.class.getSimpleName()).toFile();
        File missingNumberDir = new File(postgresqlDir, ".10");
        missingNumberDir.mkdir();
        Throwable thrown = assertThrows(PostgresqlInitialBasePathFindException.class,
            () -> {
                PostgresqlAutoPersistenceStorageConf.findBestInitialPostgresqlBasePath(postgresqlDir);
            });
        assertTrue(thrown instanceof PostgresqlInitialBasePathFindException);
        assertEquals(String.format("the postgresqlDir '%s' doesn't contain any directory matching '%s' or '%s' which can be used to find the PostgreSQL base directory with most recent version (note that whitespace and non-printable characters are not trimmed from the directory names)",
                postgresqlDir.getAbsolutePath(),
                PostgresqlAutoPersistenceStorageConf.VERSION_LE_9,
                PostgresqlAutoPersistenceStorageConf.VERSION_GE_10),
                thrown.getMessage());
    }

    @Test
    public void findBestInitialPostgresqlBasePathLE9() throws IOException,
            PostgresqlInitialBasePathFindException {
        File postgresqlDir = Files.createTempDirectory(PostgresqlAutoPersistenceStorageConfTest.class.getSimpleName()).toFile();
        File versionDir = new File(postgresqlDir, "1.23");
        versionDir.mkdir();
        File binDir = new File(versionDir, PostgresqlAutoPersistenceStorageConf.BIN_TEMPLATE);
        binDir.mkdir();
        File initdbFile = new File(binDir, "initdb");
        File postgresFile = new File(binDir, "postgres");
        File pgCtlFile = new File(binDir, "pg_ctl");
        initdbFile.createNewFile();
        postgresFile.createNewFile();
        Triple result = PostgresqlAutoPersistenceStorageConf.findBestInitialPostgresqlBasePath(postgresqlDir);
        assertEquals(new ImmutableTriple<>(initdbFile.getAbsolutePath(),
                        postgresFile.getAbsolutePath(),
                        pgCtlFile.getAbsolutePath()),
                result);
    }

    @Test
    public void findBestInitialPostgresqlBasePathGE10() throws IOException,
            PostgresqlInitialBasePathFindException {
        File postgresqlDir = Files.createTempDirectory(PostgresqlAutoPersistenceStorageConfTest.class.getSimpleName()).toFile();
        File versionDir = new File(postgresqlDir, "10");
        versionDir.mkdir();
        File binDir = new File(versionDir, PostgresqlAutoPersistenceStorageConf.BIN_TEMPLATE);
        binDir.mkdir();
        File initdbFile = new File(binDir, "initdb");
        File postgresFile = new File(binDir, "postgres");
        File pgCtlFile = new File(binDir, "pg_ctl");
        initdbFile.createNewFile();
        postgresFile.createNewFile();
        Triple result = PostgresqlAutoPersistenceStorageConf.findBestInitialPostgresqlBasePath(postgresqlDir);
        assertEquals(new ImmutableTriple<>(initdbFile.getAbsolutePath(),
                        postgresFile.getAbsolutePath(),
                        pgCtlFile.getAbsolutePath()),
                result);
    }
}